# e-Commerce Project 2021

This repository has been created for the purposes of the academic project for the e-Commerce course at CEID, University of Patras.

All rights reserved by the creators: **George Glarakis, Epameinondas Papadias**

---

## You can visit our e-shop at [shop.glarakis.eu](https://shop.glarakis.eu)

* WordPress files are located inside the ["wordpress"](/wordpress) folder
* Database export is inside the file ["kids-closetDB.sql"](/kids-closetDB.sql)
* Project Documentation can be found under the name ["Project_Documentation"](/Project_Documentation.pdf)

---

## If you wish to install locally our e-shop:
1. copy the file from the [site-installation](/site-installation) directory to your web server direcory, 
2. browse to [installer.php](/site-installation/installer.php) from your browser 
3. and follow the steps on your screen


